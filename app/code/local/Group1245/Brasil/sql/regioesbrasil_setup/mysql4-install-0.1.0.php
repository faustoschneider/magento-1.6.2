<?php

$prefix = Mage::getConfig()->getTablePrefix();

$installer = $this;
$installer->startSetup();
$installer->run("
INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'AC', 'Acre');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Acre'), ('pt_BR', LAST_INSERT_ID(), 'Acre');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'AL', 'Alagoas');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Alagoas'), ('pt_BR', LAST_INSERT_ID(), 'Alagoas');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'AP', 'Amapá');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Amapá'), ('pt_BR', LAST_INSERT_ID(), 'Amapá');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'AM', 'Amazonas');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Amazonas'), ('pt_BR', LAST_INSERT_ID(), 'Amazonas');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'BA', 'Bahia');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Bahia'), ('pt_BR', LAST_INSERT_ID(), 'Bahia');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'CE', 'Ceará');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Ceará'), ('pt_BR', LAST_INSERT_ID(), 'Ceará');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'DF', 'Distrito Federal');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Distrito Federal'), ('pt_BR', LAST_INSERT_ID(), 'Distrito Federal');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'ES', 'Espírito Santo');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Espírito Santo'), ('pt_BR', LAST_INSERT_ID(), 'Espírito Santo');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'GO', 'Goiás');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Goiás'), ('pt_BR', LAST_INSERT_ID(), 'Goiás');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'MA', 'Maranhão');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Maranhão'), ('pt_BR', LAST_INSERT_ID(), 'Maranhão');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'MT', 'Mato Grosso');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Mato Grosso'), ('pt_BR', LAST_INSERT_ID(), 'Mato Grosso');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'MS', 'Mato Grosso do Sul');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Mato Grosso do Sul'), ('pt_BR', LAST_INSERT_ID(), 'Mato Grosso do Sul');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'MG', 'Minas Gerais');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Minas Gerais'), ('pt_BR', LAST_INSERT_ID(), 'Minas Gerais');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'PA', 'Pará');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Pará'), ('pt_BR', LAST_INSERT_ID(), 'Pará');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'PB', 'Paraíba');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Paraíba'), ('pt_BR', LAST_INSERT_ID(), 'Paraíba');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'PR', 'Paraná');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Paraná'), ('pt_BR', LAST_INSERT_ID(), 'Paraná');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'PE', 'Pernambuco');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Pernambuco'), ('pt_BR', LAST_INSERT_ID(), 'Pernambuco');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'PI', 'Piauí');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Piauí'), ('pt_BR', LAST_INSERT_ID(), 'Piauí');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'RJ', 'Rio de Janeiro');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Rio de Janeiro'), ('pt_BR', LAST_INSERT_ID(), 'Rio de Janeiro');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'RN', 'Rio Grande do Norte');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Rio Grande do Norte'), ('pt_BR', LAST_INSERT_ID(), 'Rio Grande do Norte');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'RS', 'Rio Grande do Sul');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Rio Grande do Sul'), ('pt_BR', LAST_INSERT_ID(), 'Rio Grande do Sul');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'RO', 'Rondônia');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Rondônia'), ('pt_BR', LAST_INSERT_ID(), 'Rondônia');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'RR', 'Roraima');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Roraima'), ('pt_BR', LAST_INSERT_ID(), 'Roraima');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'SC', 'Santa Catarina');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Santa Catarina'), ('pt_BR', LAST_INSERT_ID(), 'Santa Catarina');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'SP', 'São Paulo');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'São Paulo'), ('pt_BR', LAST_INSERT_ID(), 'São Paulo');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'SE', 'Sergipe');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Sergipe'), ('pt_BR', LAST_INSERT_ID(), 'Sergipe');

INSERT INTO `{$prefix}directory_country_region` (`country_id`, `code`, `default_name`) VALUES 
    ('BR', 'TO', 'Tocantins');
INSERT INTO `{$prefix}directory_country_region_name` (`locale`, `region_id`, `name`) VALUES
    ('en_US', LAST_INSERT_ID(), 'Tocantins'), ('pt_BR', LAST_INSERT_ID(), 'Tocantins');");

$installer->endSetup();