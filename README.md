Versão **1.6.2** do Magento para instalação de clientes com módulos básicos.

### Quais módulos tem nessa instalação? ###
* Pagseguro (BrunoAssarisse)
* Multifrete
* Correios (PedroTeixeira)
* SMTP (Aschroder)
* Delete Orders (Mgt)
* CustomGrid's